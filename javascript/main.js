$(document).ready(function() {
	let contentArea = $("#Content");
	let sideArea = $("#Menu");

	getCategories();

	function getCategories() {
		sideArea.empty();

		$.ajax({
			url: 'http://python-servers-vtnovk529892.codeanyapp.com:5000/categories/',
			type: 'GET'
		}).done(function (data) {
			console.log(data);

			$.each(data, function(i, data) {
				renderCategories(i, data);
			})

		}).fail(function() {
			console.log("Error - API Categories");
		});
	}

	function renderCategories(i, data) {
		console.log(data.title);

		sideArea.animate(outerHeight);

		sideArea.append(
			"<span id='" + data.categoryId + "' class='category' title='" + data.title + "'><i class='fa fa-circle' aria-hidden='true'></i>" + data.title + "</span>"
		);

		contentArea.append(
			"<div id='" + data.categoryId + "' class='category' title='" + data.title + "'>" +
				"<img src='www/images/" + data.categoryId + ".svg' alt='" + data.title + "' class='svg'>" +
				"<span>" + data.title + "</span>" +
			"</div>"
		);

		$(".category").unbind().click(function () {

			let currentActive = $(this).attr('id');
			$("#Menu span.category").removeClass("active");
			$("#Menu span.category#" + currentActive).addClass("active");
			let categoryId = $(this).attr("id");
			getProducts(categoryId);
		});
	}

	function getProducts(categoryId) {
		contentArea.empty();

		$.ajax({
			url: 'http://python-servers-vtnovk529892.codeanyapp.com:5000/products/' + categoryId + '/' + 0 + '/' + 50 + '/',
			type: 'GET'
		}).done(function (data) {

			let i;
			for (i = 0; i < data.length; i++) {
				let productId = data[i]["productId"];
				let productTitle = data[i].title;
				contentArea.append(
					"<div id='" + productId + "' class='product'>" +
						"<h1>" + productTitle + "</h1>" +
						"<div class='productPreload'><img src='www/images/loader.gif' alt='Loading ...'></div>" +
					"</div>"
				);
				renderProducts(productId);
			}

		}).fail(function() {
			console.log("Error - API Products");
		});
	}

	function renderProducts(productId) {
		let priceRange = [];

		$.ajax({
			url: 'http://python-servers-vtnovk529892.codeanyapp.com:5000/offers/' + productId + '/0/20/',
			type: 'GET'
		}).done(function (data) {

			let i;
			for (i = 0; i < data.length; i++) {
				priceRange.push(data[i]["price"]);
			}

			let minPrice = Math.min.apply(null, priceRange);
			let maxPrice = Math.max.apply(null, priceRange);
			let imageUrl = "www/images/no-preview.png";

			let y;
			for (y = 0; y < data.length; y++) {
				if (data[y]["img_url"] != null) {
					imageUrl = data[y]["img_url"];
				}
			}

			$("#Content #" + productId + " .productPreload").html(
				"<div class='productImage'>" +
					"<img src='" + imageUrl + "'>" +
				"</div>" +
				"<div class='productInfo'>" +
					"<h3>" + data[0]["title"] + "</h3>" +
					"<span class='productPriceRange'>" + minPrice + " Kč" + " - " + maxPrice + " Kč" + "</span>" +
					"<span class='productsCount'>Počet různých prodejen: " + data.length + "</span>" +
				"</div>"
			);

		}).fail(function() {
			console.log("Error - API Offers");
		});

		$(".product").unbind().click(function () {
			let productId = $(this).attr("id");
			let productTitle = $(".product#" + productId + " h1").text();
			getDetail(productId, productTitle);
		});
	}

	function getDetail(productId, productTitle) {
		contentArea.empty();

		$.ajax({
			url: 'http://python-servers-vtnovk529892.codeanyapp.com:5000/offers/' + productId + '/0/30/',
			type: 'GET'
		}).done(function (data) {

			let imageUrl = "www/images/no-preview.png";

			let y;
			for (y = 0; y < data.length; y++) {
				if (data[y]["img_url"] != null) {
					imageUrl = data[y]["img_url"];
				}
			}

			let i;
			for (i = 0; i < data.length; i++) {
				contentArea.append(
					"<div class='product'>" +
						"<h1>" + productTitle + "</h1>" +
						"<div class='productImage'>" +
							"<img src='" + imageUrl + "'>" +
						"</div>" +
						"<div class='productInfo'>" +
							"<h3>" + data[i]["title"] + "</h3>" +
							"<span class='productPriceRange'>" + data[i]["price"] + " Kč" + "</span>" +
						"</div>" +
					"</div>"
				);
			}

		}).fail(function() {
			console.log("Error - API Offers");
		});
	}
});
